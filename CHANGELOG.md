# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

This project does not follow semantic versioning.

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

## [21.01] - 2021-01-07

### Added

* Do not upgrade certbot when executing the command

## [20.12] - 2020-12-01

### Fixed

* pass the proper value to RENEWED_LINEAGE environment variable

## [20.11] - 2020-11-19

### Added

* emulate certbot hooks environment variables

### Fixed

* exclude only hooks with .disable to execute hooks with .sh
* don't stop global execution when hooks return errors

## [20.08] - 2020-08-21

### Changed

* evoacme: use Let's Encrypt deploy hooks by default

### Security

## [20.06.1] - 2020-06-05

### Fixed

* fixed a bad logic for arguments parsing

## [20.06] - 2020-06-03

### Changed

* remove usage of "acme" user, root does all the work

## [19.11] - 2019-11-05

### Added

* extracted from ansible-roles.git
* add a "-V" or "--version" argument to display version
* use same logic for log/error/debug functions
* use same logic for QUIET/VERBOSE variables
